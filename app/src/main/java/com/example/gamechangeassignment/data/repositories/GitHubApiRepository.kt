package com.example.gamechangeassignment.data.repositories

import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHCommentResponse
import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHIssueResponse
import com.example.gamechangeassignment.data.datasource.apidatasource.services.GitHubApiService
import io.reactivex.Single
import retrofit2.Response
import retrofit2.Retrofit

class GitHubApiRepository(val retrofit: Retrofit?) {
    fun getIssues(): Single<Response<List<GHIssueResponse>>>? {
        val gitHubApiService: GitHubApiService = retrofit!!.create(GitHubApiService::class.java)
        return gitHubApiService.getIssues()
    }

    fun getCommentForIssue(issueNumber: Int): Single<Response<List<GHCommentResponse>>>? {
        val gitHubApiService: GitHubApiService = retrofit!!.create(GitHubApiService::class.java)
        return gitHubApiService.getCommentsForIssue(issueNumber)
    }
}