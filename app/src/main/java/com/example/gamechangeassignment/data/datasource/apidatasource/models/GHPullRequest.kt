package com.example.gamechangeassignment.data.datasource.apidatasource.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GHPullRequest(
    @SerializedName("url") val url: String?,
    @SerializedName("html_url") val html_url: String?,
    @SerializedName("diff_url") val diff_url: String?,
    @SerializedName("patch_url") val patch_url: String?
) : Parcelable {
}