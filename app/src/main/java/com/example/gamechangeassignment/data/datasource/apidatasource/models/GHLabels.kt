package com.example.gamechangeassignment.data.datasource.apidatasource.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GHLabels(
    @SerializedName("id") val id: Int?,
    @SerializedName("node_id") val node_id: String?,
    @SerializedName("url") val url: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("color") val color: String?,
    @SerializedName("default") val default: Boolean?,
    @SerializedName("description") val description: String?
) : Parcelable {
}