package com.example.gamechangeassignment.data.datasource.apidatasource.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GHIssueResponse(
    @SerializedName("url") val url: String?,
    @SerializedName("repository_url") val repository_url: String?,
    @SerializedName("labels_url") val labels_url: String?,
    @SerializedName("comments_url") val comments_url: String?,
    @SerializedName("events_url") val events_url: String?,
    @SerializedName("html_url") val html_url: String?,
    @SerializedName("id") val id: Int?,
    @SerializedName("node_id") val node_id: String?,
    @SerializedName("number") val number: Int?,
    @SerializedName("title") val title: String?,
    @SerializedName("user") val user: GHUser?,
    @SerializedName("labels") val labels: List<GHLabels>?,
    @SerializedName("state") val state: String?,
    @SerializedName("locked") val locked: Boolean?,
    @SerializedName("assignee") val assignee: GHAssignee?,
    @SerializedName("assignees") val assignees: List<GHAssignee>?,
    @SerializedName("milestone") val milestone: GHMilestone?,
    @SerializedName("comments") val comments: Int?,
    @SerializedName("created_at") val created_at: String?,
    @SerializedName("updated_at") val updated_at: String?,
    @SerializedName("closed_at") val closed_at: String?,
    @SerializedName("author_association") val author_association: String?,
    @SerializedName("pull_request") val pull_request: GHPullRequest?,
    @SerializedName("body") val body: String?
) : Parcelable {
}