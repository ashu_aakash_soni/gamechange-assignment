package com.example.gamechangeassignment.data.datasource.apidatasource.services


import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHAssignee
import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHCommentResponse
import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHIssueResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubApiService {

    @GET("firebase/firebase-ios-sdk/issues")
    fun getIssues(): Single<Response<List<GHIssueResponse>>>?

    @GET("firebase/firebase-ios-sdk/issues/{issue_no}/comments")
    fun getCommentsForIssue(@Path("issue_no") issue_no: Int): Single<Response<List<GHCommentResponse>>>?
}