package com.example.gamechangeassignment.base

import android.app.Application
import com.example.gamechangeassignment.injection.component.AppComponent
import com.example.gamechangeassignment.injection.component.DaggerAppComponent
import com.example.gamechangeassignment.injection.module.ApiModule

class MyApplication : Application() {
    companion object {
        lateinit var application: MyApplication
    }

    val component: AppComponent by lazy {
        DaggerAppComponent
            .builder()
            .apiModule(ApiModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        application = this
    }

}