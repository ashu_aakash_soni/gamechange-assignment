package com.example.gamechangeassignment.base

const val GIT_ISSUE = "GIT_ISSUE"
const val PREF_KEY_ISSUES = "PREF_KEY_ISSUES"
const val PREF_KEY_TIMESTAMP = "PREF_KEY_TIMESTAMP"