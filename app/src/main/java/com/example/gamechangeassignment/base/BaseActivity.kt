package com.example.gamechangeassignment.base

import android.app.ProgressDialog
import android.os.Bundle
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResId())
        initLayouts()
    }

    protected abstract fun getLayoutResId(): Int

    protected abstract fun initLayouts()


}