package com.example.gamechangeassignment.presentation.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gamechangeassignment.R
import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHCommentResponse
import kotlinx.android.synthetic.main.layout_item_comment.view.*

class GitCommentsAdapter(val context: Context, val data: List<GHCommentResponse>) :
    RecyclerView.Adapter<GitCommentsAdapter.CommentViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        return CommentViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.layout_item_comment,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = data?.size ?: 0

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bind(data[position])
    }

    class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvUserName = itemView.tv_username
        val tvComment = itemView.tv_comment
        fun bind(ghCommentResponse: GHCommentResponse) {
            tvUserName.text = ghCommentResponse.user?.login
            tvComment.text = ghCommentResponse.body
        }
    }
}