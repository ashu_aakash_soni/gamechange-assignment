package com.example.gamechangeassignment.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.gamechangeassignment.base.MyApplication
import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHCommentResponse
import com.example.gamechangeassignment.data.repositories.GitHubApiRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class IssueDetailViewModel : ViewModel() {
    var commentsData: MutableLiveData<List<GHCommentResponse>> = MutableLiveData()
    var webException: MutableLiveData<Throwable> = MutableLiveData()

    @set:Inject
    var gitHubApiRepository: GitHubApiRepository? = null

    init {
        MyApplication.application.component.inject(this)
    }

    fun getGitCommentsForIssue(issueNumber: Int) {
        gitHubApiRepository?.getCommentForIssue(issueNumber)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())?.subscribe({
                commentsData.postValue(it.body())
            }, {
                webException.postValue(it)
            })


    }
}