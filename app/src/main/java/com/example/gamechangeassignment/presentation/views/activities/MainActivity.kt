package com.example.gamechangeassignment.presentation.views.activities

import android.content.Intent
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gamechangeassignment.R
import com.example.gamechangeassignment.base.BaseActivity
import com.example.gamechangeassignment.base.GIT_ISSUE
import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHIssueResponse
import com.example.gamechangeassignment.listeners.IIssueClickListener
import com.example.gamechangeassignment.presentation.adaptors.GitIssueAdapter
import com.example.gamechangeassignment.presentation.viewmodels.MainViewModel
import com.example.gamechangeassignment.utils.AlertDialogUtils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    private lateinit var mainViewModel: MainViewModel
    private var issueList: ArrayList<GHIssueResponse> = arrayListOf<GHIssueResponse>()

    var adapter: GitIssueAdapter? = null

    override fun getLayoutResId() = R.layout.activity_main

    override fun initLayouts() {
        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        subscribeToObservables()
        initializeViews()
        webCallToGetIssues()
    }

    private fun initializeViews() {
        adapter = GitIssueAdapter(this, issueList)
        adapter!!.iIssueClickListener = object : IIssueClickListener {
            override fun onIssueItemClick(position: Int, itemData: GHIssueResponse) {
                val intent = Intent(this@MainActivity, IssueDetailActivity::class.java)
                intent.putExtra(GIT_ISSUE, itemData)
                startActivity(intent)
            }
        }
        rv_issues.layoutManager = LinearLayoutManager(this)
        rv_issues.adapter = adapter
    }

    private fun subscribeToObservables() {
        mainViewModel.issueData.observe(this, Observer {
            hideLoader()
            issueList.clear()
            issueList.addAll(it)
            adapter?.notifyDataSetChanged()
        })

        mainViewModel.webException.observe(this, Observer {
            it.printStackTrace()
            AlertDialogUtils.showAlert(
                this,
                getString(R.string.error),
                getString(R.string.error_msg)
            )
        })
    }

    private fun webCallToGetIssues() {
        showLoader()
        mainViewModel.getGitHubIssues()
    }

    private fun showLoader() {
        progressBar_cyclic.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        progressBar_cyclic.visibility = View.GONE
    }
}
