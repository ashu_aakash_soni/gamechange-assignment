package com.example.gamechangeassignment.presentation.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gamechangeassignment.R
import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHIssueResponse
import com.example.gamechangeassignment.listeners.IIssueClickListener
import kotlinx.android.synthetic.main.layout_item_issue.view.*

class GitIssueAdapter(val context: Context, val data: List<GHIssueResponse>) :
    RecyclerView.Adapter<GitIssueAdapter.IssueViewHolder>() {
    var iIssueClickListener: IIssueClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IssueViewHolder {
        return IssueViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.layout_item_issue,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = data?.size ?: 0

    override fun onBindViewHolder(holder: IssueViewHolder, position: Int) {
        holder.bind(data[position], iIssueClickListener)
    }

    class IssueViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvHeading = itemView.tv_heading
        val tvDescription = itemView.tv_description

        fun bind(
            ghIssueResponse: GHIssueResponse,
            iIssueClickListener: IIssueClickListener?
        ) {
            tvHeading.text = ghIssueResponse.title
            tvDescription.text = ghIssueResponse.body

            itemView.setOnClickListener(View.OnClickListener {
                iIssueClickListener?.onIssueItemClick(adapterPosition, ghIssueResponse)
            })
        }
    }
}