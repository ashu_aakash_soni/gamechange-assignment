package com.example.gamechangeassignment.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.gamechangeassignment.base.MyApplication
import com.example.gamechangeassignment.base.PREF_KEY_ISSUES
import com.example.gamechangeassignment.base.PREF_KEY_TIMESTAMP
import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHIssueResponse
import com.example.gamechangeassignment.data.repositories.GitHubApiRepository
import com.example.gamechangeassignment.utils.SharedPreferenceHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.reflect.Type
import javax.inject.Inject


class MainViewModel : ViewModel() {

    var issueData: MutableLiveData<List<GHIssueResponse>> = MutableLiveData()
    var webException: MutableLiveData<Throwable> = MutableLiveData()
    var twentyFourHourMili: Long = 86400000L

    @set:Inject
    var gitHubApiRepository: GitHubApiRepository? = null

    @set:Inject
    var sharedPreferenceHelper: SharedPreferenceHelper? = null

    @set:Inject
    var gson: Gson? = null

    init {
        MyApplication.application.component.inject(this)
    }

    fun getGitHubIssues() {
        if (isDataMoreThanOneDayOld()) {
            getGitHubIssuesFromApi()
        } else {
            val list = getIssuesFromPref()
            if (!list.isNullOrEmpty()) {
                issueData.postValue(list)
            }
        }

    }

    private fun getGitHubIssuesFromApi() {
        gitHubApiRepository?.getIssues()?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())?.subscribe({
                it.body()?.let { it1 ->
                    saveIssuesToPref(it1)
                    issueData.postValue(it1)
                }

            }, {
                webException.postValue(it)
            })
    }

    private fun saveIssuesToPref(list: List<GHIssueResponse>) {
        var dataString = gson?.toJson(list)
        sharedPreferenceHelper?.putString(PREF_KEY_ISSUES, dataString)
        sharedPreferenceHelper?.putLong(PREF_KEY_TIMESTAMP, System.currentTimeMillis())
    }

    private fun getIssuesFromPref(): List<GHIssueResponse>? {
        val type: Type = object : TypeToken<List<GHIssueResponse?>?>() {}.type
        val dataString = sharedPreferenceHelper?.getString(PREF_KEY_ISSUES)
        return gson?.fromJson<List<GHIssueResponse>>(dataString, type)
    }

    private fun isDataMoreThanOneDayOld(): Boolean {
        val lastSaveTimeStamp = sharedPreferenceHelper?.getLong(PREF_KEY_TIMESTAMP)
        return if (lastSaveTimeStamp == 0L)
            true
        else System.currentTimeMillis() - lastSaveTimeStamp!! > twentyFourHourMili

    }
}