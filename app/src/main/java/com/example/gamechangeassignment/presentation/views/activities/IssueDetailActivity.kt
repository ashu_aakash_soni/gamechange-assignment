package com.example.gamechangeassignment.presentation.views.activities

import android.content.Intent
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gamechangeassignment.R
import com.example.gamechangeassignment.base.BaseActivity
import com.example.gamechangeassignment.base.GIT_ISSUE
import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHCommentResponse
import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHIssueResponse
import com.example.gamechangeassignment.presentation.adaptors.GitCommentsAdapter
import com.example.gamechangeassignment.presentation.viewmodels.IssueDetailViewModel
import com.example.gamechangeassignment.utils.AlertDialogUtils
import kotlinx.android.synthetic.main.activity_issue_detail.*


class IssueDetailActivity : BaseActivity() {
    private lateinit var issueDetailViewModel: IssueDetailViewModel
    private var ghIssueResponse: GHIssueResponse? = null
    private var commentsList: ArrayList<GHCommentResponse> = arrayListOf<GHCommentResponse>()
    var adapter: GitCommentsAdapter? = null

    override fun getLayoutResId() = R.layout.activity_issue_detail

    override fun initLayouts() {
        initToolBar()
        issueDetailViewModel = ViewModelProvider(this).get(IssueDetailViewModel::class.java)
        subscribeToObservables()
        initializeViews()
        ghIssueResponse?.let {
            webCallToGetCommentsForIssue(it.number!!)
        }

    }

    private fun initToolBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Issue"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initializeViews() {
        ghIssueResponse = intent.getParcelableExtra<GHIssueResponse>(GIT_ISSUE)
        tv_title.text = ghIssueResponse?.title
        tv_description.text = ghIssueResponse?.body

        adapter = GitCommentsAdapter(this, commentsList)
        rv_comments.layoutManager = LinearLayoutManager(this)
        rv_comments.adapter = adapter
    }

    private fun subscribeToObservables() {
        issueDetailViewModel.commentsData.observe(this, Observer {
            hideLoader()
            commentsList.clear()
            commentsList.addAll(it)
            adapter?.notifyDataSetChanged()

            if (it.isEmpty()) {
                AlertDialogUtils.showAlert(
                    this,
                    getString(R.string.no_comments),
                    getString(R.string.no_comments_msg)
                )
            }
        })


        issueDetailViewModel.webException.observe(this, Observer {
            it.printStackTrace()
            AlertDialogUtils.showAlert(
                this,
                getString(R.string.error),
                getString(R.string.error_msg)
            )
        })
    }

    private fun webCallToGetCommentsForIssue(issueNo: Int) {
        showLoader()
        issueDetailViewModel.getGitCommentsForIssue(issueNo)
    }

    private fun showLoader() {
        progressBar_cyclic.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        progressBar_cyclic.visibility = View.GONE
    }
}
