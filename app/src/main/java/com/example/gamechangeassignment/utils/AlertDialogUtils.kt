package com.example.gamechangeassignment.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog

class AlertDialogUtils {
    companion object {
        fun showAlert(context: Context, title: String, message: String) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setPositiveButton("OK") { dialogInterface, which ->

            }
            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }
}