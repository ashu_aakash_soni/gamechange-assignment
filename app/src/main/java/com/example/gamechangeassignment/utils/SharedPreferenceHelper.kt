package com.example.gamechangeassignment.utils

import android.content.SharedPreferences

class SharedPreferenceHelper(val sharedPreferences: SharedPreferences?) {

    fun putString(key: String, value: String?) {
        val editor = sharedPreferences?.edit()
        editor?.putString(key, value)
        editor?.commit()
    }

    fun putLong(key: String, value: Long) {
        val editor = sharedPreferences?.edit()
        editor?.putLong(key, value)
        editor?.commit()
    }

    fun getLong(key: String): Long? {
        return sharedPreferences?.getLong(key, 0L)
    }

    fun getString(key: String): String? {
        return sharedPreferences?.getString(key, "")
    }
}