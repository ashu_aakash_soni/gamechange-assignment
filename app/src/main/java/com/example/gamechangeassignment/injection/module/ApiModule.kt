package com.example.gamechangeassignment.injection.module

import android.content.Context
import android.content.SharedPreferences
import com.example.gamechangeassignment.BuildConfig
import com.example.gamechangeassignment.data.repositories.GitHubApiRepository
import com.example.gamechangeassignment.utils.SharedPreferenceHelper
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import java.util.logging.Logger
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule(val applicationContext: Context) {
    private val connectionTimeout = 30 // 30 sec
    private val apiRetryCount = 3

    @Provides
    @Singleton
    @Named("networkTimeoutInSeconds")
    fun provideNetworkTimeoutInSeconds() = connectionTimeout

    @Provides
    @Singleton
    @Named("retryCount")
    fun provideApiRetryCount() = apiRetryCount


    @Provides
    @Singleton
    @Named("base_url")
    fun provideBaseUrl(): String? {
        return "https://api.github.com/repos/"
    }

    @Provides
    @Singleton
    @Named("preference_name")
    fun provideSharedPreferenceFileName(): String? {
        return "myPreference"
    }

    @Provides
    @Singleton
    @Named("isDebug")
    fun provideIsDebug(): Boolean {
        return BuildConfig.DEBUG
    }

    @Singleton
    @Provides
    @Named("loggingInterceptor")
    fun loggingInterceptor(logger: Logger?): Interceptor? {
        return Interceptor { chain: Interceptor.Chain ->
            val request = chain.request()
            val t1 = System.nanoTime()
            logger!!.info(
                String.format(
                    "Sending request %s on %s%n%s",
                    request.url(), chain.connection(), request.headers()
                )
            )
            val response = chain.proceed(request)
            val t2 = System.nanoTime()
            logger!!.info(
                String.format(
                    "Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6, response.headers()
                )
            )
            response
        }
    }

    @Provides
    @Singleton
    fun provideLogger(): Logger? {
        return Logger.getAnonymousLogger()
    }

    @Provides
    @Singleton
    fun provideGsonConverterFactoryNormal(): Converter.Factory? {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun provideRxJavaCallAdapterFactory(): CallAdapter.Factory? {
        return RxJava2CallAdapterFactory.create()
    }

    @Singleton
    @Provides
    fun provideOkHttpClientWithAuthorization(
        @Named("loggingInterceptor") loggingInterceptor: Interceptor?,
        @Named("networkTimeoutInSeconds") networkTimeoutInSeconds: Int,
        @Named("isDebug") isDebug: Boolean
    ): OkHttpClient? {
        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(networkTimeoutInSeconds.toLong(), TimeUnit.SECONDS)
        //show logs if app is in Debug mode
        if (isDebug) {
            okHttpClient.addInterceptor(loggingInterceptor)
        }
        return okHttpClient.build()
    }

    @Provides
    @Singleton
    fun provideAuthenticatedRetrofitNormal(
        @Named("base_url") baseUrl: String?, converterFactory: Converter.Factory?,
        callAdapterFactory: CallAdapter.Factory?, okHttpClient: OkHttpClient?
    ): Retrofit? {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(callAdapterFactory)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideGitHubApiRepository(retrofit: Retrofit?): GitHubApiRepository? {
        return GitHubApiRepository(retrofit)
    }

    @Provides
    @Singleton
    fun provideSharedPreference(@Named("preference_name") preference_name: String?): SharedPreferences? {
        return applicationContext.getSharedPreferences(preference_name, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideSharedPreferenceHelper(sharedPreferences: SharedPreferences?): SharedPreferenceHelper? {
        return SharedPreferenceHelper(sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson? {
        return Gson()
    }
}