package com.example.gamechangeassignment.injection.component

import com.example.gamechangeassignment.injection.module.ApiModule
import com.example.gamechangeassignment.presentation.viewmodels.IssueDetailViewModel
import com.example.gamechangeassignment.presentation.viewmodels.MainViewModel
import com.example.gamechangeassignment.presentation.views.activities.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class])
interface AppComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(mainViewModel: MainViewModel)

    fun inject(issueDetailViewModel: IssueDetailViewModel)
}