package com.example.gamechangeassignment.listeners

import com.example.gamechangeassignment.data.datasource.apidatasource.models.GHIssueResponse

interface IIssueClickListener {
    fun onIssueItemClick(position: Int, itemData: GHIssueResponse)
}